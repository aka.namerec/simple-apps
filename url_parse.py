import collections
import re
from typing import NamedTuple, OrderedDict, Union
from urllib.parse import unquote


class UrlParts(NamedTuple):
    schema: str
    host: str
    port: str
    path: str
    parameters: OrderedDict[str, str]
    anchor: str


_RX_URL = re.compile(
    r'^(?P<schema>\w+)://' +
    r'(?P<host>.+?)' +
    r'(:(?P<port>\d+?))?' +
    r'(/(?P<path>[^?#]+?))?' +
    r'(\?(?P<params>.+?))?' +
    r'(#(?P<anchor>.*))?$'
)


def url_parse(url: str) -> Union[UrlParts, None]:
    match = _RX_URL.match(url)
    if not match:
        return None
    groups = match.groupdict()

    def safe_get_group(parameter: str, perform_unquote=True) -> str:
        result = groups.get(parameter) or ''
        return result if not perform_unquote else unquote(result)

    def extract_params(params: str) -> OrderedDict[str, str]:
        result = collections.OrderedDict()
        for param_spec in params.split('&'):
            if param_spec:
                param_name, param_value = param_spec.split('=')
                result[unquote(param_name)] = unquote(param_value)
        return result if len(result) > 0 else None

    return UrlParts(
        schema=safe_get_group('schema'),
        host=safe_get_group('host'),
        port=safe_get_group('port'),
        path=safe_get_group('path'),
        parameters=extract_params(safe_get_group('params', False)),
        anchor=safe_get_group('anchor')
    )


TEST_URLS = (
    'http://yandex.ru:500/wiki/page/12/?param=10#anchor',
    ('http://yandex.ru:500/wiki/page/12/?param1=10&param2=%D0%A2%D0%B5%D1%81%D1%82+%2B+%D1%81%D0%BF%D0%B5%D1%86' +
     '%D0%B8%D0%B0%D0%BB%D1%8C%D0%BD%D1%8B%D1%85+%26+%D1%81%D0%B8%D0%B2%D0%BE%D0%BB%D0%BE%D0%B2+%3D+%D0%B8.%D1%82.'+
     '%D0%B4.&param3=%D0%B5%D1%89%D1%91+%D0%BF%D0%B0%D1%80%D0%B0%D0%BC%D0%B5%D1%82%D1%80'),
    'http://yandex.ru/wiki/page/12/?param=10&param1=25#anchor1',
    'http://yandex.ru/wiki/page/12#anchor1',
    'http://yandex.ru/anchor1',
    ('https://ru.wikipedia.org/wiki/%D0%96%D1%83%D0%BA%D0%BE%D0%B2,_%D0%93%D0%B5%D0%BE%D1%80%D0%B3%D0%B8%D0%B9_%D0' +
     '%9A%D0%BE%D0%BD%D1%81%D1%82%D0%B0%D0%BD%D1%82%D0%B8%D0%BD%D0%BE%D0%B2%D0%B8%D1%87'),
    ('https://ru.wikipedia.org/wiki/%D0%96%D1%83%D0%BA%D0%BE%D0%B2,_%D0%93%D0%B5%D0%BE%D1%80%D0%B3%D0%B8%D0%B9_%D0' +
     '%9A%D0%BE%D0%BD%D1%81%D1%82%D0%B0%D0%BD%D1%82%D0%B8%D0%BD%D0%BE%D0%B2%D0%B8%D1%87#%D0%9C%D0%B8%D0%BD%D0%B8%D1' +
     '%81%D1%82%D1%80_%D0%BE%D0%B1%D0%BE%D1%80%D0%BE%D0%BD%D1%8B_%D0%A1%D0%A1%D0%A1%D0%A0'),
)

for url in TEST_URLS:
    print(f'* url: "{url}"')
    print(f'  {url_parse(url)}')
