from ipaddress import IPv4Address, IPv6Address, IPv4Network, IPv6Network, IPV4LENGTH, IPV6LENGTH
from typing import Union

IPAddress = Union[IPv4Address, IPv6Address, None]


class Subnets(object):
    """ This class provided to evaluate minimal subnets for set of IP addresses """

    def __init__(self):
        self.__subnet_address: Union[bytearray, None] = None
        self.__prefix_len: int = 0
        self.__prefix_len_bytes: int = 0
        self.__prefix_tail_bits_left: int = 0
        self.ip_address_class, self.network_class = None, None

    def setup(self, address: IPAddress):
        assert address, 'Address must be assigned.'
        self.__subnet_address = bytearray(address.packed)
        self.prefix_len = IPV6LENGTH if isinstance(address, IPv6Address) else IPV4LENGTH
        self.ip_address_class = address.__class__
        self.network_class = IPv6Network if self.ip_address_class == IPv6Address else IPv4Network

    @property
    def subnet_address(self):
        # No strict mode, because we using `self.__subnet_address` for internal purposes
        return self.network_class((bytes(self.__subnet_address), self.prefix_len), strict=False)

    @property
    def prefix_len(self):
        return self.__prefix_len

    @prefix_len.setter
    def prefix_len(self, prefix_len):
        self.__prefix_len = prefix_len
        used_tail_bits: int = self.__prefix_len % 8  # the bits count, used by current network mask in it's last byte
        self.__prefix_len_bytes = (self.__prefix_len // 8) + (1 if used_tail_bits > 0 else 0)

        # self .__ prefix_tail_bits_left - the number of bits in the last byte of the netmask, which should be taken
        # into account when analyzing the differences in its last byte.
        self.__prefix_tail_bits_left = (7 - used_tail_bits) if used_tail_bits > 0 else 0
        self.__subnet_address[self.__prefix_len_bytes - 1] |= 1 << self.__prefix_tail_bits_left

    def consider(self, address: IPAddress):
        if not address:
            return
        if not self.__subnet_address:
            self.setup(address)
            return
        if not isinstance(address, self.ip_address_class):
            raise TypeError(f'The argument must be instance of {self.ip_address_class}.')

        current: bytearray = self.__subnet_address
        test: bytes = address.packed
        for i in range(0, self.__prefix_len_bytes):
            c: int
            t: int
            c, t = current[i], test[i]
            if c != t:
                # Scan address byte, which contains difference to calculate new prefix length.
                # If difference detected in last byte, used by subnet mask, check only first "left" bits, used by
                # subnet mask.
                tail_range: int = self.__prefix_tail_bits_left if i == self.__prefix_len_bytes - 1 else 0
                for j in range(7, tail_range, -1):
                    test_bit: int = 1 << j
                    if c & test_bit != t & test_bit:
                        # Subnet mask must be shorter on 1 relative difference position:
                        # 8 * i: prefix bits in identical bytes in `current` and `test`
                        # 8 - (j + 1) => 7 - j: prefix bits in identical part of different last byte
                        self.prefix_len = 8 * i + 7 - j
                        return
