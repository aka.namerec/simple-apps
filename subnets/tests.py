import unittest
from ipaddress import IPv4Address, IPv6Address

from subnets import Subnets


class TestSubnets(unittest.TestCase):

    def test_invalid_usage(self):

        def raise_type_error():
            s = Subnets()
            s.consider(IPv4Address('127.0.0.1'))
            s.consider(IPv6Address('ecda:fb32:fc3e::'))

        self.assertRaises(TypeError, raise_type_error)

    def assert_prefix_len(self, subnets: Subnets, length: int):
        self.assertEqual(subnets.prefix_len, length)

    def test_consider_v4(self):
        s = Subnets()
        s.consider(IPv4Address('192.168.0.0'))
        self.assert_prefix_len(s, 32)
        s.consider(IPv4Address('192.168.0.1'))
        self.assert_prefix_len(s, 32)
        s.consider(IPv4Address('192.168.0.2'))
        self.assert_prefix_len(s, 30)
        s.consider(IPv4Address('192.168.0.3'))
        self.assert_prefix_len(s, 30)
        s.consider(IPv4Address('192.168.0.4'))
        self.assert_prefix_len(s, 29)
        s.consider(IPv4Address('192.168.10.5'))
        self.assert_prefix_len(s, 20)
        s.consider(IPv4Address('192.168.10.128'))
        self.assert_prefix_len(s, 20)
        s.consider(IPv4Address('158.12.35.2'))
        self.assert_prefix_len(s, 1)
        self.assertEqual(str(s.subnet_address), '128.0.0.0/1')
        s.consider(IPv4Address('1.10.32.150'))
        self.assert_prefix_len(s, 0)

    def test_consider_v6(self):
        s = Subnets()
        s.consider(IPv6Address('ff80::0123:1234:ef00'))
        self.assert_prefix_len(s, 128)
        s.consider(IPv6Address('ff80::0123:1234:ef01'))
        self.assert_prefix_len(s, 128)
        s.consider(IPv6Address('ff80::0123:1234:ef12'))
        self.assert_prefix_len(s, 123)
        s.consider(IPv6Address('ff80::0123:1234:ef07'))
        self.assert_prefix_len(s, 123)
        s.consider(IPv6Address('ff80::0123:1234:da12'))
        self.assert_prefix_len(s, 114)
        s.consider(IPv6Address('ff80::0123:1234:0012'))
        self.assert_prefix_len(s, 112)
        s.consider(IPv6Address('ff80::0123:1234:da3c'))
        self.assert_prefix_len(s, 112)
        s.consider(IPv6Address('ff80::fa23:1234:da12'))
        self.assert_prefix_len(s, 80)
        s.consider(IPv6Address('faec::fa23:1234:da12'))
        self.assert_prefix_len(s, 5)
        self.assertEqual(str(s.subnet_address), 'f800::/5')
        s.consider(IPv6Address('01ec::fa23:1234:da12'))
        self.assert_prefix_len(s, 0)


if __name__ == '__main__':
    unittest.main()
