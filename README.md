# Simple apps

Simple applications for my code demonstration.

# Requirements

Some apps required Python 3.8+.

# Apps

* [`sparse_matrix.py`](./sparse_matrix.py): [Sparse matrix](https://en.wikipedia.org/wiki/Sparse_matrix).
* [`text_statistics.py`](./text_statistics.py): Small statistics for text file: number of unique words, number of words encountered once. 
* [`url_parse.py`](./url_parse.py): Regexp for parse URLs.
* [`subnets/subnents.py`](./subnets/subnets.py): Calculate minimal subnet for set of IP addresses. Both IPv4 and IPv6 addresses are supported.
