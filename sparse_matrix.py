class SparseMatrix(dict):
    def __init__(self, dimension, null_value=0):
        super().__init__()
        self.dimension = dimension
        self.null_value = null_value

    def __getitem__(self, key):
        self.validate_index(key)
        return self.get(key, self.null_value)

    def __setitem__(self, key, value):
        self.validate_index(key)
        self.pop(key) if value == self.null_value else super().__setitem__(key, value)

    def validate_index(self, index):
        assert isinstance(index, tuple), f'Index may be a only tuple with length {self.dimension}.'
        assert len(index) == self.dimension, (
            f'Index for this matrix must be a tuple of {self.dimension} elements. You specified: {index}.'
        )


def test():
    import random

    def rnd_index():
        return tuple([random.randrange(0, 10**6, 1) for x in range(0, 5)])

    matrix = SparseMatrix(5)

    index = (1, 2, 3, 4, 5)
    matrix[index] = 1024
    assert matrix[(1, 2, 3, 4, 5)] == 1024
    matrix[(1, 2, 3, 4, 5)] = 0

    count = 10**6
    print(f'Testing the matrix for {count} elements:')
    indexes = [rnd_index() for x in range(0, count)]

    real_count = 0
    for index in indexes:
        if matrix[index] == 0:
            real_count += 1
        matrix[index] = index
    print(f'Count of non-null uniquee elements in matrix: {len(matrix.keys())}')
    assert len(matrix.keys()) == real_count

    for index in indexes:
        assert matrix[index] == index
        matrix[index] = 0
    assert len(matrix.keys()) == 0
    print('Done.')


if __name__ == '__main__':
    test()
