import io
import os
import re
import sys
from typing import IO, NamedTuple, Dict, Set, List

# [begin-region] [text_stat.py] This code may be moved to separate module.

RX_ALL_WORDS = re.compile(r'\w+')


def file_words(fp: IO) -> str:
    # read a file line by line to be able to process huge texts
    while line := fp.readline():
        for word in RX_ALL_WORDS.findall(line):
            yield word


class TextStatistics(NamedTuple):
    words_count: int
    words_set: Set[str]
    words_dict: Dict[str, int]

    @property
    def words_encountered_once(self) -> List[str]:
        return list(map(lambda x: x[0], filter(lambda x: x[1] == 1, self.words_dict.items())))


def get_text_statistics(fp: IO) -> TextStatistics:
    words_dict = dict()
    words_set = set()
    words_count = 0
    for word in file_words(fp):
        words_count += 1
        words_set.add(word)
        words_dict[word] = words_dict.setdefault(word, 0) + 1
    return TextStatistics(words_count=words_count, words_set=words_set, words_dict=words_dict)


# [end-region] [text_stat.py]

# [begin-region] [test_text_stat.py] This code can be moved to separate test module (e.g. PyTest case)

CONTENTS = """\
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus libero nulla, ultrices vitae suscipit a, viverra ac 
nisl. Donec a suscipit dui. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec tempus ante nunc, id 
auctor magna rutrum vel. Donec dignissim sapien id ex venenatis eleifend. Praesent vitae nisl quis massa imperdiet 
molestie at quis mi. Donec euismod porttitor neque a condimentum. Maecenas arcu mauris, sollicitudin id dictum eu, 
tempor nec ligula. Pellentesque hendrerit quis nulla vel placerat. In scelerisque sagittis eros. Quisque aliquam 
consequat ipsum. Morbi sodales, nibh in posuere facilisis, sapien mauris convallis odio, non maximus lacus sapien et 
augue. In ac neque sem. Nam imperdiet sem eros, ac rutrum elit suscipit quis. Vestibulum mollis lorem quis aliquam 
accumsan. Phasellus nec aliquet erat.\
"""


def run_tests():
    statistics = get_text_statistics(io.StringIO(CONTENTS))
    assert (statistics.words_count == 127)
    assert (len(statistics.words_set) == len(statistics.words_dict.keys()) == 89)
    assert (statistics.words_dict['quis'] == 5)
    assert (statistics.words_dict['Donec'] == 4)
    assert (statistics.words_dict['a'] == 3)
    assert (statistics.words_dict['viverra'] == 1)
    print('Things are good.')


# [end-region] [test_text_stat.py]

# [beging-region] From this and below - main app code

class EScriptError(Exception):
    """ I like when a program has one entry point, and one exit point. This exception helps me with this. """
    pass


def usage():
    USAGE = (
            'This program is collect small statistics for text file: number of the words, number of unique words, ' +
            'number of words encountered once, etc.\n' +
            f'Usage: {os.path.basename(__file__)} ' + '{<file_name>|--test}'
    )
    raise EScriptError(USAGE)


def execute(source_name):
    source_name = os.path.abspath(source_name)
    if not os.path.exists(source_name):
        raise EScriptError(f'File "{source_name}" does not exist.')
    statistics = get_text_statistics(open(source_name, 'r'))
    print(f'* File "{source_name}":')
    print(f'* The total words count: {statistics.words_count}.')
    print(f'* The number of unique words: {len(statistics.words_set)}.')
    # print(sorted(list(words_set)))
    print(f'* The number of words encountered once: {len(statistics.words_encountered_once)}.')
    # print(sorted(statistics.words_encountered_once))


def main():
    if len(sys.argv) != 2:
        usage()
    param = sys.argv[1]
    if param == '--test':
        run_tests()
    else:
        execute(param)


if __name__ == '__main__':
    try:
        main()
    except EScriptError as e:
        print(str(e))
